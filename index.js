const inputIcon = document.querySelector(".icon-password");
const confirmIcon = document.querySelector(".icon-confirm");
const passwordInput = document.querySelector("#password");
const passwordConfirm = document.querySelector("#password_confirm");
const  button = document.querySelector("button");
const wrong = document.createElement("p");

inputIcon.onclick = function () {
    if (passwordInput.getAttribute("type") === "password"){
        passwordInput.setAttribute("type","text");
        inputIcon.classList.remove("fa-eye");
        inputIcon.classList.add("fa-eye-slash");

    }else{
        passwordInput.setAttribute("type","password");
        inputIcon.classList.remove("fa-eye-slash");
        inputIcon.classList.add("fa-eye");
    }
}
confirmIcon.onclick = function () {
    if (passwordConfirm.getAttribute("type") === "password"){
        passwordConfirm.setAttribute("type","text");
        confirmIcon.classList.remove("fa-eye");
        confirmIcon.classList.add("fa-eye-slash");
    }else{
        passwordConfirm.setAttribute("type","password");
        confirmIcon.classList.remove("fa-eye-slash");
        confirmIcon.classList.add("fa-eye");
    }
}
button.onclick = function () {
    if (passwordInput.value !== passwordConfirm.value){
        wrong.textContent = "Потрібно ввести однакові значення";
        wrong.style.color = "red";
        document.querySelector(".wrong_password").appendChild(wrong);
    }else if (passwordInput.value === passwordConfirm.value && document.querySelector(".wrong_password").contains(wrong)){
        document.querySelector(".wrong_password").removeChild(wrong);
        alert("You are welcome")
    }else{
        alert("You are welcome");
    }
}




